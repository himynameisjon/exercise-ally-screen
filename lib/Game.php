<?php namespace Ikariam;

class Game {
    private static $avatar;
    private $view;
    private $params;

    public function __construct() {
        self::$avatar = new Avatar();
    }

    public static function getAvatar() {
        return self::$avatar;
    }

    public function getAvatarId() {
        return self::$avatar->getId();
    }

    public function setView(string $view) {
        $this->view = $view;
    }

    public function setViewParameters(array $params) {
        $this->params = $params;
    }

    public function addFeedback() {}
}