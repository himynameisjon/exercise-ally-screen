<?php namespace Ikariam;

class Constants
{
    public function __construct()
    {
        define('FEEDBACK_LOCATION_DIPLOMACY_ADVISOR', 'FEEDBACK_LOCATION_DIPLOMACY_ADVISOR');
        define('ECM_02_BASIC_ERROR', 'ECM_02_BASIC_ERROR');
        define('ECM_CATEGORY_NON_FIXABLE', 'ECM_CATEGORY_NON_FIXABLE');
        define('FEEDBACK_TYPE_SUCCESSFUL', 'FEEDBACK_TYPE_SUCCESSFUL');
        define('FEEDBACK_LOCATION_BUTTON', 'FEEDBACK_LOCATION_BUTTON');
        define('ALLY_TEXT_EXTERIOR', 'ALLY_TEXT_EXTERIOR');
    }
}
