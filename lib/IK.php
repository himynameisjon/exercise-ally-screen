<?php namespace Ikariam;

class IK {
    public static function game() {
        return new Game();
    }

    public static function mysqli() {
        return new DB;
    }

    public static function lang() {
        return new Lang();
    }

    public static function fillMobileErrorCode() {}
}