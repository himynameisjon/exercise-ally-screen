<?php namespace Ikariam;

class Lang {
    public function get(string $key) {
        return "Localized-{$key}";
    }
}