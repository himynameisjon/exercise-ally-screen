<?php namespace Ikariam;

class Ally
{
    protected $props = array();
 
    public function __construct()
    {
        $this->setId('ally-id');
        $this->setName('ally-name');
        $this->setTag('ally-tag');
    }

    public static function found()
    {
        return true;
    }

    public function dissolve()
    {
        return true;
    }

    public function __call($methodName, $params = null)
    {
        $position = $this->findPropPosition($methodName);
        $methodPrefix = substr($methodName, 0, $position);
        $key = strtolower(substr($methodName, $position));

        switch ($methodPrefix) {
            case 'edit':
            case 'set':
            case 'add':
                // ugly magic to keep interface the same and yet avoid having to write getters and setters
                $value = end($params);
                $this->props[$key] = $value;
                return $this;
            case 'get':
                return $this->props[$key] ?? null;
            default:
                throw new \Exception("Method {$methodName} is undefined");
        }
    }

    private function findPropPosition(string $methodName)
    {
        return strcspn($methodName, implode('', range('A', 'Z')));
    }

    public function save()
    {
    }
    
    public function calculateDiplomaticInfluence()
    {
    }
}
