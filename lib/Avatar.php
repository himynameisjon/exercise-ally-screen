<?php namespace Ikariam;

class Avatar
{
    private $id;
    private $ally;

    public function __construct()
    {
        $this->id = 'avatar-id';
        $this->ally = new Ally();
    }

    public function getId()
    {
        return $this->id;
    }

    public function getAlly()
    {
        if (!$this->ally) {
            throw new NoAllyException('This avatar has no ally');
        }

        return $this->ally;
    }

    public function calculateDiplomaticInfluence()
    {
    }
}
