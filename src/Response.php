<?php namespace Ikariam;

class Response
{
    private $game;

    public function __construct() {
        $this->game = IK::game();
    }

    public function updateViewProps(Props\ScreenPropsInterface $screen)
    {
        try {
            $this->setView($screen->getView());
            $this->setViewParams($screen->getParams());
            $this->addFeedback($screen->getFeedback());
            $this->addMobileError($screen->getMobileError());
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    private function setView(string $view) {
        return $this->game->setView($view);
    }

    private function setViewParams(array $params) {
        return $this->game->setViewParameters($params);
    }

    private function addFeedback(Props\FeedbackPropsInterface $feedback) {
        return $this->game->addFeedback($feedback->getMessage(), $feedback->getPosition(), $feedback->getType());
    }

    private function addMobileError(Props\MobileErrorPropsInterface $error) {
        return IK::fillMobileErrorCode($error->getType(), $error->getCategory(), $error->getMessage());
    }
}
