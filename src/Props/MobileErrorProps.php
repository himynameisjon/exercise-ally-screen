<?php namespace Ikariam\Props;

class MobileErrorProps implements MobileErrorPropsInterface
{
    private $type;
    private $category;
    private $message;

    public function __construct(string $type, string $category, string $message)
    {
        $this->type = $type;
        $this->position = $position;
        $this->message = $message;
    }

    public function getType()
    {
        return $this->type;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function getMessage()
    {
        return $this->message;
    }
}
