<?php namespace Ikariam\Props;

interface ScreenPropsInterface
{
    public function getView(): string;
    public function getParams(): array;
    public function getFeedback(): FeedbackPropsInterface;
    public function getMobileError(): MobileErrorPropsInterface;
}