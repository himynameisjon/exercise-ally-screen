<?php namespace Ikariam\Props;

interface MobileErrorPropsInterface
{
    public function getType();
    public function getCategory();
    public function getMessage();
}
