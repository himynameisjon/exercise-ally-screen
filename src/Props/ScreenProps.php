<?php namespace Ikariam\Props;

class ScreenProps implements ScreenPropsInterface
{
    private $view;
    private $params;
    private $feedback;
    private $mobileError;

    public function __construct(string $view, array $params = [], FeedbackPropsInterface $feedback = null, MobileErrorPropsInterface $mobileError = null)
    {
        $this->view = $view;
        $this->params = $params;
        $this->feedback = $feedback;
        $this->mobileError = $mobileError;
    }

    public function getView(): string
    {
        return $this->view;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function getFeedback(): FeedbackPropsInterface
    {
        return $this->feedback;
    }

    public function getMobileError(): MobileErrorPropsInterface
    {
        return $this->mobileError;
    }
    
}
