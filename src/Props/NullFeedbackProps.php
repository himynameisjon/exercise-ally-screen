<?php namespace Ikariam\Props;

class NullFeedbackProps implements FeedbackPropsInterface
{
    private $message;
    private $position;
    private $type;

    public function getMessage()
    {
        return $this->message;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function getType()
    {
        return $this->type;
    }
}
