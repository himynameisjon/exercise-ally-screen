<?php namespace Ikariam\Props;

class NullMobileErrorProps implements MobileErrorPropsInterface
{
    private $type;
    private $category;
    private $message;

    public function getType()
    {
        return $this->type;
    }

    public function getCategory()
    {
        return $this->category;
    }

    public function getMessage()
    {
        return $this->message;
    }
}
