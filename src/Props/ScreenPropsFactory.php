<?php namespace Ikariam\Props;

use Ikariam\IK;

class ScreenPropsFactory
{
    private $lang;

    public function __construct()
    {
        $this->lang = IK::lang();
    }

    public function getError(string $message): ScreenPropsInterface
    {
        return new ScreenProps(
            'noViewChange',
            [],
            new FeedbackProps($message, FEEDBACK_LOCATION_DIPLOMACY_ADVISOR),
            new MobileErrorProps(ECM_02_BASIC_ERROR, ECM_CATEGORY_NON_FIXABLE, $message)
        );
    }

    public function getErrorChangeAlly(): ScreenPropsInterface
    {
        return $this->getError($this->lang->get('TXT_SERVERMERGE_CANNOT_CHANGE_ALLY'));
    }

    public function getErrorChangeAllyLeader(): ScreenPropsInterface
    {
        return $this->getError($this->lang->get('TXT_SERVERMERGE_CANNOT_CHANGE_ALLY_LEADER'));
    }

    public function getErrorAccessDenied(): ScreenPropsInterface
    {
        return new ScreenProps(
            'noViewChange',
            [],
            new FeedbackProps($this->lang->get('TXT_ERROR_ACCESS_DENIED')),
            new MobileErrorProps(ECM_02_BASIC_ERROR, ECM_CATEGORY_NON_FIXABLE, IK::lang()->get('TXT_ERROR_ACCESS_DENIED'))
        );
    }

    public function getSuccess(array $params, $view = 'embassy'): ScreenPropsInterface
    {
        return new ScreenProps(
            $view,
            $params,
            new FeedbackProps($this->lang->get('TXT_ACTION_SUCCESSFUL'), FEEDBACK_LOCATION_DIPLOMACY_ADVISOR, FEEDBACK_TYPE_SUCCESSFUL),
            new NullMobileErrorProps()
        );
    }

    public function getSuccessWithoutFeedback(array $params = [])
    {
        return new ScreenProps(
            'noViewChange',
            $params,
            new FeedbackProps('', FEEDBACK_LOCATION_BUTTON, FEEDBACK_TYPE_SUCCESSFUL),
            new NullMobileErrorProps()
        );
    }

    public function getEditAlly(array $params = [])
    {
        return new ScreenProps(
            'editAllyPage',
            $params,
            new NullFeedbackProps(),
            new NullMobileErrorProps()
        );
    }
}
