<?php namespace Ikariam\Props;

interface FeedbackPropsInterface
{
    public function getMessage();
    public function getPosition();
}
