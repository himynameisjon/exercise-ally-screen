<?php namespace Ikariam\Props;

class FeedbackProps implements FeedbackPropsInterface
{
    private $message;
    private $position;
    private $type;

    public function __construct(string $message, string $position, string $type)
    {
        $this->message = $message;
        $this->position = $position;
        $this->type = $type;
    }

    public function getMessage()
    {
        return $this->message;
    }

    public function getPosition()
    {
        return $this->position;
    }

    public function getType() {
        return $this->type;
    }
}
