<?php namespace Ikariam;

use Ikariam\Servermerge;
use Ikariam\Game;
use Ikariam\Props\ScreenPropsFactory;

class AllyScreen
{
    private $game;
    private $avatar;
    private $request;
    private $response;
    private $viewPropsFactory;

    public function __construct(Game $game, Request $request, Response $response, ScreenPropsFactory $factory)
    {
        $this->game = $game;
        $this->avatar = $game->getAvatar();
        $this->request = $request;
        $this->response = $response;
        $this->viewPropsFactory = $factory;
    }

    /**
     * Create new ally
     *
     */
    public function found()
    {
        $params = $this->request->getValues([ Request::TYPE_DEFAULT, Request::TYPE_DESCRIPTION, ]);

        try {
            $this->verifyNoServerMerge($this->avatar->getId());
            $this->verifyAllyFound($params);
        } catch (InvalidAccessException $e) {
            return $this->response->updateViewProps($this->viewPropsFactory->getErrorChangeAlly());
        } catch (Exception $e) {
            return $this->response->updateViewProps($this->viewPropsFactory->getError($result->getCode()));
        }

        return $this->response->updateViewProps($this->viewPropsFactory->getSuccess([
            'cityId'    => $params['cityId'],
            'position'  => $params['position'],
            'activeTab' => 'tabEmbassy',
        ]));
    }

    private function verifyNoServerMerge($avatarId): bool
    {
        if ((new Servermerge())->reservationHasPlayerChoosenATargetServer($avatarId)) {
            throw new InvalidAccessException('Cannot take action while server merge is running');
        }

        return true;
    }

    private function verifyAllyFound($params)
    {
        if (!Ally::found(IK::mysqli(), $this->game->getAvatar(), $params['name'], $params['tag'], $params['shortdesc'])) {
            throw new \Exception("Ally {$params['name']} was not found");
        }
    }

    public function dissolve()
    {
        $params = $this->request->getValues([Request::TYPE_DEFAULT]);

        try {
            $ally   = $this->avatar->getAlly();
            $this->verifyNoServerMerge($this->avatar->getId());
            $ally->dissolve($this->avatar);
        } catch (InvalidAccessException $e) {
            return $this->response->updateViewProps($this->viewPropsFactory->getErrorAccessDenied());
        } catch (Exception $e) {
            return $this->response->updateViewProps($this->viewPropsFactory->getError($e->getCode()));
        }

            return $this->response->updateViewProps($this->viewPropsFactory->getSuccess($params));
    }

    public function edit()
    {
        $params = $this->request->getValues([ Request::TYPE_DEFAULT, Request::TYPE_DESCRIPTION, Request::TYPE_CONCISE, ]);

        try {
            $ally = $this->avatar->getAlly();
            $sourceName = $ally->getName();
            $this->verifyNoServerMerge($this->avatar->getId());
            $this->updateAllyName($ally, $this->avatar, $params);
            $ally->save();
            $this->logAllyName($this->avatar, $ally->getId(), $sourceName, $ally->getName());
        } catch (InvalidAccessException $e) {
            return $this->response->updateViewProps($this->viewPropsFactory->getErrorAccessDenied());
        } catch (Exception $e) {
            return $this->response->updateViewProps($this->viewPropsFactory->getError($e->getCode()));
        }

        return $this->response->updateViewProps($this->viewPropsFactory->getSuccess([
            'cityId' => $params['cityId'],
            'position' => $params['position'],
            'successfulChange' => 1
        ]));
    }

    private function updateAllyName($ally, $avatar, $params)
    {
        if (empty($params['nameAndTagOnly'])) {
            $ally->setShortdesc($avatar, $params['shortdesc']);
        }

        $ally->setName($avatar, $params['name']);
        $ally->setTag($avatar, $params['name']);
    }

    private function logAllyName($avatar, string $allyId, string $old, string $new)
    {
        AnalyticsFactory::getInstance()->logAllyRename($avatar, $allyId, $old, $new);
    }

    public function editPosts()
    {
        $params = $this->request->getValues([ Request::TYPE_DEFAULT, Request::TYPE_PEOPLE, ]);

        try {
            $ally = $this->avatar->getAlly();
            $ally->setGeneralId($this->avatar, $params['general']);
            $ally->setDiplomatId($this->avatar, $params['diplomat']);
            $ally->setHomeSecretaryId($this->avatar, $params['homeSecretary']);
            $ally->save();
        } catch (InvalidAccessException $e) {
            return $this->response->updateViewProps($this->viewPropsFactory->getErrorAccessDenied());
        } catch (Exception $e) {
            return $this->response->updateViewProps($this->viewPropsFactory->getError($e->getCode()));
        }

        return $this->response->updateViewProps($this->viewPropsFactory->getSuccess($params, 'embassyCharges'));
    }

    public function editLeader()
    {
        $params = $this->request->getValues([ Request::TYPE_DEFAULT, Request::TYPE_LEADER, ]);

        try {
            $ally = $this->avatar->getAlly();

            $reservedMerge = HappeningList::getInstance()->get('NewServermergeReservationHappeningEffect');

            if ($reservedMerge instanceof NewServermergeReservationHappeningEffect && $reservedMerge->isServerSourceServer()) {
                return $this->response->updateViewProps($this->viewPropsFactory->getErrorChangeAllyLeader());
            }

            $ally->setLeaderId($this->avatar, $params['leader']);
        } catch (InvalidAccessException $e) {
            return $this->response->updateViewProps($this->viewPropsFactory->getErrorAccessDenied());
        } catch (Exception $e) {
            return $this->response->updateViewProps($this->viewPropsFactory->getError($e->getCode()));
        }

        return $this->response->updateViewProps($this->viewPropsFactory->getSuccess($params));
    }


    public function assignDi()
    {
        $params = $this->request->getValues([ Request::TYPE_DEFAULT, Request::TYPE_DI, ]);

        try {
            $ally = $this->avatar->getAlly();

            $this->avatar->calculateDiplomaticInfluence();
            $ally->calculateDiplomaticInfluence();
            $ally->setLeaderDiAssigned($this->avatar, $params['di']);
        } catch (Exception $e) {
            return $this->response->updateViewProps($this->viewPropsFactory->getError($e->getCode()));
        }

        return $this->response->updateViewProps($this->viewPropsFactory->getSuccessWithoutFeedback($params));
    }

    public function editText()
    {
        $params = $this->request->getValues([ Request::TYPE_DEFAULT, Request::TYPE_TEXT, ]);

        try {
            $ally = $this->avatar->getAlly();

            if ($params['isPreview']) {
                return $this->response->updateViewProps($this->viewPropsFactory->getEditAlly($params));
            }

            if ($params['type'] == ALLY_TEXT_EXTERIOR) {
                $ally->setHomepage($this->avatar, $params['homepage']);
            }

            $ally->editTextExterior($this->avatar, $params['text']);
            $ally->save();
        } catch (InvalidAccessException $e) {
            return $this->response->updateViewProps($this->viewPropsFactory->getErrorAccessDenied());
        } catch (Exception $e) {
            return $this->response->updateViewProps($this->viewPropsFactory->getError($e->getCode()));
        }

        return $this->response->updateViewProps($this->viewPropsFactory->getSuccessWithoutFeedback($params));
    }

    public function addRank()
    {
        // 1. convert to request values
        $cityId = HelpFunctions::getRequestValue("cityId");
        $pos    = HelpFunctions::getRequestValue("position");

        $data['rposition']  = HelpFunctions::getRequestValue("rposition");
        $data['rautomatic'] = HelpFunctions::getRequestValue("rautomatic");
        $data['rankName']   = HelpFunctions::getRequestValue("rankName", 'playerName');

        $data['opAutomatic']   = HelpFunctions::getRequestValue("opAutomatic", 'plainstring') === 'on';
        $data['opDegrade']     = HelpFunctions::getRequestValue("opDegrade", 'plainstring') === 'on';
        $data['opUpgrade']     = HelpFunctions::getRequestValue("opUpgrade", 'plainstring') === 'on';
        $data['opKick']        = HelpFunctions::getRequestValue("opKick", 'plainstring') === 'on';
        $data['opMail']        = HelpFunctions::getRequestValue("opMail", 'plainstring') === 'on';
        $data['opStatus']      = HelpFunctions::getRequestValue("opStatus", 'plainstring') === 'on';
        $data['opSpecialRank'] = HelpFunctions::getRequestValue("opSpecialRank", 'plainstring') === 'on';
        $data['opMemberCity']  = HelpFunctions::getRequestValue("opMemberCity", 'plainstring') === 'on';
        $data['opTreaty']      = HelpFunctions::getRequestValue("opTreaty", 'plainstring') === 'on';
        $data['spawn']         = HelpFunctions::getRequestValue("spawn");
        $data['opForumRights'] = HelpFunctions::getRequestValue("opForumRights");

        // 2. replace $avatar with calls to $this->avatar
        $avatar = IK::game()->getAvatar();
        $ally   = $avatar->getAlly();

        // 3. add response for missing input
        if ($data['opAutomatic'] == true && $data['rautomatic'] < 1) {
            IK::game()->addFeedback(IK::lang()->get('TXT_ERROR_FORM_MISSING_INPUT'));
            IK::game()->setView("noViewChange");
            IK::fillMobileErrorCode(ECM_02_BASIC_ERROR, ECM_CATEGORY_NON_FIXABLE,
            IK::lang()->get('TXT_ERROR_FORM_MISSING_INPUT'));
            return;
        }

        // 4. catch access denied errors
        if (!($ally instanceof Ally)) {
            IK::game()->addFeedback(IK::lang()->get('TXT_ERROR_ACCESS_DENIED'));
            IK::game()->setView("noViewChange");
            IK::fillMobileErrorCode(ECM_02_BASIC_ERROR, ECM_CATEGORY_NON_FIXABLE,
            IK::lang()->get('TXT_ERROR_ACCESS_DENIED'));
            return;
        }

        // 5. convert this and rautomatic to exception handler
        if (!$ally->addRank($avatar, $data)) {
            IK::game()->addFeedback(IK::lang()->get('TXT_ERROR_FORM_MISSING_INPUT'));
            IK::game()->setView("noViewChange");
            IK::fillMobileErrorCode(ECM_02_BASIC_ERROR, ECM_CATEGORY_NON_FIXABLE,
            IK::lang()->get('TXT_ERROR_FORM_MISSING_INPUT'));
            return;
        }

        // 6. was this an attempt at memory management?
        $data = null;

        // 7. return success response
        IK::game()->addFeedback(IK::lang()->get("TXT_ACTION_SUCCESSFUL"), FEEDBACK_LOCATION_DIPLOMACY_ADVISOR,
        FEEDBACK_TYPE_SUCCESSFUL);
        IK::game()->setView("embassyEditRanks");
        IK::game()->setViewParameters(array("cityId" => $cityId, "position" => $pos));

        return true;
    }
}
