<?php namespace Ikariam;

class Request
{
    const TYPE_DEFAULT = 'default';
    const TYPE_DESCRIPTION = 'description';
    const TYPE_CONCISE = 'concise';
    const TYPE_ALLY = 'ally';
    const TYPE_PEOPLE = 'people';
    const TYPE_LEADER = 'leader';
    const TYPE_RANK = 'rank';
    const TYPE_RANK_FILTER = 'rank-filter';
    const TYPE_WITH = 'with';
    const TYPE_DI = 'di';
    const TYPE_TEXT = 'text';

    const REQUEST_TYPE_CONFIG = [
        self::TYPE_DEFAULT => [
            'cityId' => null,
            'position' => null,
        ],
        self::TYPE_DESCRIPTION => [
            'name' => 'string',
            'tag' => 'string',
            'shortdesc' => 'html',
        ],
        self::TYPE_CONCISE => [
            'nameAndTagOnly' => null,
        ],
        self::TYPE_PEOPLE => [
            'general' => 'int',
            'diplomat' => 'int',
            'homeSecretary' => 'int',
        ],
        self::TYPE_LEADER => [
            'leader' => 'int',
        ],
        self::TYPE_RANK => [
            'rID' => null,
        ],
        self::TYPE_RANK_FILTER => [
            'rposition' => null,
            'rautomatic' => null,
            'spawn' => null,
            'rankName' => 'playerName',
            'opDegrade' => 'boolean',
            'opUpgrade' => 'boolean',
            'opKick' => 'boolean',
            'opMail' => 'boolean',
            'opStatus' => 'boolean',
            'opSpecialRank' => 'boolean',
            'opMemberCity' => 'boolean',
            'opAutomatic' => 'boolean',
            'opTreaty' => 'boolean',
            'opForumRights' => null,
        ],
        self::TYPE_WITH => [
            'with' => null,
        ],
        self::TYPE_ALLY => [
            'allyId' => null,
        ],
        self::TYPE_DI => [
            'di' => 'int',
        ],
        self::TYPE_TEXT => [
            'type' => 'alnum',
            'text' => 'plainstring',
            'homepage' => 'string',
            'isPreview' => null,
        ],
    ];

    public function getConfig(array $types)
    {
        return array_reduce($types, function ($accumulator, $type) {
            if (!self::REQUEST_TYPE_CONFIG[$type]) {
                throw new UnknownTypeException("Request value configuration type {$type} unknown");
            }

            return $accumulator + self::REQUEST_TYPE_CONFIG[$type];
        }, array());
    }

    public function getValues(array $types)
    {
        $params = $this->getConfig($types);

        array_walk($params, function (string &$type = null, string $key = '') {
            $type = $this->getValue($key, $type);
        });

        return $params;
    }

    private function getValue(string $key, string $type = null)
    {
        switch ($type) {
            case 'html':
                return $this->replaceHtml(HelpFunctions::getRequestValue($key, $type));
            case 'boolean':
                return HelpFunctions::getRequestValue($key, 'plainstring') === 'on';
            default:
                return HelpFunctions::getRequestValue($key, $type);
        }
    }

    private function replaceHtml($atext)
    {
        return $atext;
    }
}
