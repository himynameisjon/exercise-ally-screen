<?php declare(strict_types=1);
namespace Ikariam;

use Ikariam\AllyScreen;
use PHPUnit\Framework\TestCase;
use Ikariam\Props\ScreenPropsFactory;

new Constants();

class AllyScreenTest extends TestCase
{
    protected $screen;

    protected function setUp()
    {
        $this->screen = new AllyScreen(IK::game(), new Request(), new Response(), new ScreenPropsFactory());
    }

    public function testClassCanBeCreated(): void
    {
        $this->assertInstanceOf(AllyScreen::class, $this->screen);
    }

    public function testCanCallFoundAction(): void
    {
        $this->assertTrue($this->screen->found());
    }

    public function testCanCallDissolveAction(): void
    {
        $this->assertTrue($this->screen->dissolve());
    }

    public function testCanCallEditAction(): void
    {
        $this->assertTrue($this->screen->edit());
    }

    public function testCanCallEditPostsAction(): void
    {
        $this->assertTrue($this->screen->editPosts());
    }

    public function testCanCallEditLeaderAction(): void
    {
        $this->assertTrue($this->screen->editLeader());
    }

    public function testCanCallAssignDiAction(): void
    {
        $this->assertTrue($this->screen->assignDi());
    }

    public function testCanCallEditTextAction(): void
    {
        $this->assertTrue($this->screen->editText());
    }

    public function testCanCallAddRankAction(): void
    {
        $this->assertTrue($this->screen->addRank());
    }
}
