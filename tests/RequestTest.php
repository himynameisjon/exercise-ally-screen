<?php declare(strict_types=1);
namespace Ikariam;

use Ikariam\Request;
use PHPUnit\Framework\TestCase;

class RequestTest extends TestCase
{
    protected $request;

    protected function setUp()
    {
        $this->request = new Request();
    }

    public function testClassCanBeCreated(): void
    {
        $this->assertInstanceOf(Request::class, $this->request);
    }

    public function testCanGetEmptyConfig(): void
    {
        $this->assertEmpty($this->request->getConfig([]));
    }

    public function testCanGetDefaultConfig(): void
    {
        $this->assertArrayHasKey('cityId', $this->request->getConfig(['default']));
        $this->assertArrayHasKey('position', $this->request->getConfig(['default']));
    }
}
